from flask import Flask, request
import re

validador = Flask(__name__)


class ValidadorEmail:
    @validador.route('/validador-email', methods=['GET'])
    def validate_email():
        email = request.args.get('email')
        if not email:
            return 'Por favor, forneça um endereço de e-mail', 400
        if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
            return 'Endereço de e-mail inválido', 400
        return 'Endereço de e-mail válido', 200


if __name__ == "__main__":
    validador.run(debug=True)
