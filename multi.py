from flask import Flask, request
import unittest

multi = Flask(__name__)


class Multi:
    @multi.route('/multi', methods=['GET'])
    def multiplicacao():
        a = request.args.get('a')
        b = request.args.get('b')
        if not a or not b:
            return 'Por favor, forneça ambos os números', 400
        try:
            result = int(a) * int(b)
            return str(result), 200
        except ValueError:
            return 'Os parâmetros fornecidos devem ser números inteiros', 400


if __name__ == '__main__':
    unittest.main()
