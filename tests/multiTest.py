from flask import Flask, request
import unittest

multi = Flask(__name__)


class TestMultiplicacao(unittest.TestCase):

    def setUp(self):
        self.multi = multi.test_client()

    def test_multiplicacao(self):
        response = self.multi.get('/multiplicacao?num1=2&num2=3')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.decode(), '6')

    def test_parametro_faltante(self):
        response = self.multi.get('/multiplicacao?num1=2')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data.decode(),
                         'Por favor, forneça ambos os números')

    def test_parametro_invalido(self):
        response = self.multi.get('/multipicacao?num1=2&num2=abc')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data.decode(), 'Os parâmetros fornecidos devem ser números inteiros')


if __name__ == '__main__':
    unittest.main()
