import unittest
from validador_email import validador


class TestValidadorEmail(unittest.TestCase):

    def setUp(self):
        self.validador = validador.test_client()

    def test_email_valido(self):
        response = self.validador.get(
            '/validador-email?email=test@example.com')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.decode(), 'Endereço de e-mail válido')

    def test_email_invalido(self):
        response = self.validador.get('/validador-email?email=invalid-email')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data.decode(),
                         'Endereço de e-mail inválido')

    def test_parametro_faltante(self):
        response = self.validador.get('/validador-email')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data.decode(),
                         'Por favor, forneça um endereço de e-mail')


if __name__ == '__main__':
    unittest.main()
